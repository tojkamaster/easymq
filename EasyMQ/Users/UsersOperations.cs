﻿using EasyMQ.SecurityUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EasyMQ.Users
{
    class UsersOperations
    {
        SqlConnection sqlConnection;

        public UsersOperations(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
        }

        /*
         * -1  - Zła nazwa użytkownika lub hasło
         * 0   - Użytkownik jest nieaktywny
         * 1   - Użytkownik zalogowany poprawnie
         */
        public int LoginUser(string userName, string password)
        {
            string sql = @"
                SELECT * FROM Users WHERE UserName = @UserName AND UserPasswordHash = @UserPasswordHash
            ";

            SqlCommand cmd = new SqlCommand(sql, this.sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@UserName", userName));
            cmd.Parameters.Add(new SqlParameter("@UserPasswordHash", Sha256Util.sha256_hash(password)));
            

            using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleRow))
            {
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        if ((bool)rdr["UserEnabled"] == false)
                        {
                            return 0;
                        }
                        else return 1;
                    }
                }
                else return -1;
            }
            return -1;
        }

        /*
         * Zwraca ID dodanego użytkownika
         */
        public int AddUser(string userName, string password, bool enabled, string email, bool superuser)
        {
            string sql = @"
                INSERT INTO Users(UserName, UserPasswordHash, UserEnabled, UserEmail, UserSuperUser)
                VALUES(@UserName, @UserPasswordHash, @UserEnabled, @UserEmail, @UserSuperUser);

                SELECT @@IDENTITY;
            ";

            SqlCommand cmd = new SqlCommand(sql, this.sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@UserName", userName));
            cmd.Parameters.Add(new SqlParameter("@UserPasswordHash", Sha256Util.sha256_hash(password)));
            cmd.Parameters.Add(new SqlParameter("@UserEnabled", enabled));
            cmd.Parameters.Add(new SqlParameter("@UserEmail", email));
            cmd.Parameters.Add(new SqlParameter("@UserSuperUser", superuser));

            int userId = Convert.ToInt32(cmd.ExecuteScalar());
            return userId;
        }
    }
}
