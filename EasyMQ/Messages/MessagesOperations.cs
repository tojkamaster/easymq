﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyMQ.Messages
{
    class MessagesOperations
    {
        SqlConnection sqlConnection;

        public MessagesOperations(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
        }

        public int AddMessage(int queueId, string message)
        {
            byte[] theBytes = Encoding.UTF8.GetBytes(message);

            string sql = @"
                INSERT INTO Messages(QueueId, Message, MessageSize) VALUES(@QueueId, @Message, @MessageSize);
                
                SELECT @@IDENTITY;
            ";
            SqlCommand cmd = new SqlCommand(sql, sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@QueueId", queueId));
            cmd.Parameters.Add(new SqlParameter("@Message", theBytes));
            cmd.Parameters.Add(new SqlParameter("@MessageSize", theBytes.Length));

            int id = Convert.ToInt32(cmd.ExecuteScalar());
            return id;
        }
    }
}
