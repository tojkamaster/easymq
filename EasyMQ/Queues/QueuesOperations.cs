﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyMQ.Queues
{
    class QueuesOperations
    {
        SqlConnection sqlConnection;

        public QueuesOperations(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
        }

        public int AddQueue(int userId, string name, bool enabled)
        {
            string sql = @"
                INSERT INTO Queues(UsersId, QueueName, QueueEnabled)
                VALUES(@UserId, @QueueName, @QueueEnabled);

                SELECT @@IDENTITY;
            ";

            SqlCommand cmd = new SqlCommand(sql, this.sqlConnection);
            cmd.Parameters.Add(new SqlParameter("@userId", userId));
            cmd.Parameters.Add(new SqlParameter("@QueueName", name));
            cmd.Parameters.Add(new SqlParameter("@QueueEnabled", enabled));

            int id = Convert.ToInt32(cmd.ExecuteScalar());
            return id;
        }


    }
}
