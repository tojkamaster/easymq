﻿using EasyMQ.Messages;
using EasyMQ.Queues;
using EasyMQ.Users;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyMQ
{
    class Program
    {
        static void Main(string[] args)
        {
            //C:\Users\Admin\source\repos\EasyMQ\EasyMQ\emqdata.mdf
            SqlConnection sqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\31100122\source\repos\easymq\easymq\EasyMQ\emqdata.mdf;Integrated Security=True");
            sqlConnection.Open();

            //byte[] theBytes = Encoding.UTF8.GetBytes("To jest wiadomość");

            //SqlCommand cmd = new SqlCommand("INSERT INTO Messages(Message) VALUES(@Message)", sqlConnection);
            //cmd.Parameters.Add(new SqlParameter("@Message", theBytes));
            ////cmd.ExecuteNonQuery();

            //cmd = new SqlCommand("SELECT * FROM Messages", sqlConnection);
            //using(SqlDataReader reader = cmd.ExecuteReader())
            //{
            //    while (reader.Read())
            //    {
            //        string MHEX = BitConverter.ToString((byte[])reader["Message"]);

            //        Console.WriteLine(MHEX);
            //        Console.WriteLine("0x" + MHEX.Replace("-", ""));
            //        Console.WriteLine(Encoding.UTF8.GetString((byte[])reader["Message"]));
            //    }
            //}

            //Console.WriteLine("DZIALAA");
            //sqlConnection.Close();

            UsersOperations uo = new UsersOperations(sqlConnection);
            //uo.LoginUser("TOjek", "QWEqwe123123");
            //uo.LoginUser("TOjek", "QWEqwe123123");
            //uo.LoginUser("TOjek", "abcdef");

            Console.WriteLine(uo.AddUser("bobok", "QWEqwe123123", false, "bobok@bobok.pl", false));
            Console.WriteLine(uo.AddUser("bobok2", "QWEqwe123123", true, "bobok2@bobok.pl", false));

            Console.WriteLine(uo.LoginUser("bobok", "Q"));
            Console.WriteLine(uo.LoginUser("bobok", "QWEqwe123123"));
            Console.WriteLine(uo.LoginUser("bobok2", "QWEqwe123123"));

            QueuesOperations qo = new QueuesOperations(sqlConnection);
            Console.WriteLine(qo.AddQueue(12, "QUEUE.PRODUCTION.DATA", true));

            MessagesOperations mo = new MessagesOperations(sqlConnection);
            Console.WriteLine(mo.AddMessage(1, "To jest wiadomosc z kolejki"));

            sqlConnection.Close();
            Console.ReadLine();

        }
    }
}
